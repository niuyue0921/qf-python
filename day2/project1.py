## python中的标准数据类型
"""
六种
数值 字符串 列表 元组 集合 字典
是否可变
可变： 列表 集合 字典
不可变： 数值 字符串 元组
是否有序
有序: 字符串 列表 元组
无序：集合 字典
是否可重复
可以: 字符串 列表 元组 
不可以: 集合 字典的键
除了数值都是
"""

# 数值类型
'''
a = 1
b = 1.0
print(type(a), type(b))

# 各种进制转换十进制
print(0b10110) # 二进制数
print(0o75547) # 八进制数
print(0xaf16b) # 十六进制数

# 十进制转换其他进制
print(bin(4))
print(oct(8))
print(hex(16))
'''

# 随机方法
'''
import random
## 随机取值
list1 = ["张三", "李四", "王五", "赵六"]
print(random.choice(list1))

## 从范围内取值
print(random.randrange(100000,999999))

## 随机取一个0-1的实数
print(random.random())

## 随机排序
list1 = ["张三", "李四", "王五", "赵六"]
random.shuffle(list1)
print(list1)
'''

# 字符串类型
# 见jupyter文档1

for i in range(0x4e00, 0x9fa6):
    print(chr(i), end=" ")
    