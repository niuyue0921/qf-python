from lxml import etree
import requests
import time
from tqdm import tqdm

header = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36 Edg/126.0.0.0",
    "Referer": "https://www.bqka.cc/book/876/"
}


next_url = "https://www.bqka.cc/book/876/1.html"

with tqdm(total=10) as pbar:
    for i in range(10):
        r = requests.get(url=next_url, headers=header)
        if r.status_code == 200:
            e = etree.HTML(r.text)
            # 小说章节标题
            title = e.xpath('//div/span[@class="title"]/text()')[0]
            text = "\n".join(e.xpath('//div[@id="chaptercontent"]/text()'))
            next_url = "https://www.bqka.cc" + e.xpath('//a[text()="下一章"]/@href')[0]
            with open("修罗武神.txt", "a", encoding="utf-8") as f:
                f.write("\n"+title+"\n")
                f.write(text)
            pbar.set_description(title)
            pbar.update(1)
        else:
            print("网络请求失败")
        time.sleep(1)