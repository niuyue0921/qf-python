# python if 判断
"""
python 中使用缩进表示层级关系

a = 10
if a < 10:
    print(True)
elif a < 5:
    pass
else:
    print(False)
"""
"""
user_name = "xiaoming"
password = "123456"
user, passwd = input("请输入用户名："), input("请输入密码：")
if user == user_name:
    if passwd == password:
        print("登录成功")
    else:
        print("密码错误")
else:
    print("用户不存在")

if user == user_name and passwd == password:
    print("登录成功")
else:
    print("账号密码错误")
"""

# python 中的循环
"""
while 判断条件
for  判断范围
关键字 break continue
语法糖
"""
# for 循环可以遍历的数据为可迭代对象 迭代器、字符串、列表、元组、集合、字典
"""
range 可以把数字变为迭代器，python的范围左闭右开区间
range(100): 0-99
range(5,100): 5-99
range(5,100,2): 5-99 步长为2

"""

# for i in range(1, 100, 2):
#     print(i)

# for i in "hello":
#     print(i)

# while 循环
'''
a = 0
while a < 10:
    a += 1 # a=a+1
    if a == 5:
        break
    print(a)
else:
    print("循环执行完成")
'''