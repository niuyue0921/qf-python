# 1、使用while循环输出 1 2 3 4 5 6   8 9 10
# 计数器 
'''
a = 0
while a < 10:
    a += 1
    if a != 7:
        print(a,end=" ")
    else:
        print(" ", end=' ')
'''

# 2、求1-100的所有数的和
'''
a = 0
for i in range(101):
    a += i # a = a + i
print(a)
'''

# 3、输出 1-100 内的所有奇数
'''
for i in range(1,100,2):
    print(i)

for i in range(1,100):
    if i % 2 == 1:
        print(i)
'''
# 4、求1-2+3-4+5 ... 99的所有数的和
a = 0
for i in range(100):
    if i % 2 == 1:
        a += i
    else:
        a -= i
print(a)
# 5、用户登陆（三次机会重试）
'''
u = "xiaoming"
p = "123"
for i in range(3):
    username, password = input("请输入账号："),input("请输入密码：")
    if u == username and p == password:
        print('登录成功')
        break
    else:
        print("登录失败，请重试")
'''

# 6、打印九九乘法表 shell python 

# 外: 1-9   1       2
# 内: 1-外  1x1=1   1x2=2 2x2=4
'''
for i in range(1,10):
    for j in range(1,i+1):
        print(f"{j}x{i}={j*i}", end="\t")
    print()

for i in {1..9}
do
        for j in `seq 1 $i`
        do
                echo -n "${j}x${i}=$[$j * $i] "
        done
        echo 
done
'''